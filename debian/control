Source: kdecoration
Section: libs
Priority: optional
Maintainer: Debian Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
Uploaders: Scarlett Moore <sgmoore@kde.org>,
           Patrick Franz <patfra71@gmail.com>,
           Norbert Preining <norbert@preining.info>
Build-Depends: cmake,
               debhelper-compat (= 13),
               gettext,
               kf6-extra-cmake-modules,
               kf6-kcoreaddons-dev,
               kf6-ki18n-dev,
               pkg-kde-tools-neon,
               qt6-base-dev
Standards-Version: 4.6.2
Homepage: https://invent.kde.org/plasma/kdecoration
Vcs-Browser: https://salsa.debian.org/qt-kde-team/kde/kdecoration
Vcs-Git: https://salsa.debian.org/qt-kde-team/kde/kdecoration.git
Rules-Requires-Root: no

Package: libkdecorations3-6
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, ${shlibs:Depends}
Breaks: libkdecorations2-5v5 (<< 4:6.3.0), libkdecorations2-6 (<< 4:6.3.0)
Replaces: libkdecorations2-5v5 (<< 4:6.3.0), libkdecorations2-6 (<< 4:6.3.0)
Description: library to create window decorations
 KDecoration2 is a library to create window decorations. These window
 decorations can be used by for example an X11 based window manager
 which re-parents a Client window to a window decoration frame.

Package: libkdecorations3-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: libkdecorations3-6 (= ${binary:Version}),
         libkdecorations3private2 (= ${binary:Version}),
         qt6-base-dev,
         ${misc:Depends}
Description: library to create window decorations - development files
 KDecoration2 is a library to create window decorations. These window
 decorations can be used by for example an X11 based window manager
 which re-parents a Client window to a window decoration frame.
 .
 This package contains the development files.

Package: libkdecorations3private2
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, ${shlibs:Depends}
Breaks: libkdecorations3private1(<< ${source:Version}~ciBuild)
Replaces: libkdecorations3private1 (<< ${source:Version}~ciBuild)
Description: library to create window decorations - private library
 KDecoration2 is a library to create window decorations. These window
 decorations can be used by for example an X11 based window manager
 which re-parents a Client window to a window decoration frame.
 .
 This package contains the private library parts that are not considered stable.

Package: libkdecorations2-6
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, ${shlibs:Depends}
Priority: extra
Section: oldlibs
Description: library to create window decorations - transitional
 Empty dummy package

Package: libkdecorations2-5v5
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, ${shlibs:Depends}
Priority: extra
Section: oldlibs
Description: library to create window decorations - transitional
 Empty dummy package

Package: libkdecorations3private1
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, ${shlibs:Depends}
Priority: extra
Section: oldlibs
Description: library to create window decorations - transitional
 Empty dummy package